import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of }         from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';

import { ResultObject } from './result-object';
import { ParamObject } from './param-object';

export interface CustomHttpReply {
    results: ResultObject[];
    next: string|null;
    previous: string|null;
    count: number;
}


@Injectable()
export class InfiniteLoaderService {
    param: ParamObject;
    private _url: string  | undefined;
    private _urlNext: string|null = null;
    private _urlPrevious: string|null = null;
    private _ordering: any;
    private _reachedPrevious: boolean = false;
    private _reachedNext: boolean = false;

    constructor(private http: HttpClient) { 
    }

    init(param?: ParamObject): void {
        if (param) {
            this.param = param;
        } else {
            this.param = new ParamObject();
        }
        if (this.param.ordering === undefined) {
            this.param.ordering = "id";
        }
        if (this.param.reverseOrdering === undefined) {
            this.param.reverseOrdering = this.param.ordering.substring(0,1) === "-"? this.param.ordering.substring(1) : "-" + this.param.ordering;
        }
        this._url = undefined;
        this._urlNext = null;
        this._urlPrevious = null;
        this._reachedPrevious = false;
        this._reachedNext = false;
    }

    setUrl(url:string): void {
        this._url = url;
    }

    get url() : string {
        return this._url;
    }

    put(obj:ResultObject) : Promise<any> {
        return this.http.put<ResultObject>(this._url + obj.id.toString() + "/", obj).toPromise();
    }

    delete(obj:ResultObject) : Promise<any> {
        return this.http.delete<any>(this._url + obj.id.toString() + "/").toPromise();
    }

    private getGreaterThanParam(): string {
        if (this.param.ordering !== undefined) {

            if (this.param.ordering.substring(0,1) === "-") {
                return this.param.reverseOrdering + "_lt";
            } else {
                return this.param.ordering + "_gt";
            }
        } else {
            return "id_gt";
        }
    }

    private getLessThanParam(): string {
        if (this.param.ordering !== undefined) {
            if (this.param.ordering.substring(0,1) === "-") {
                return this.param.reverseOrdering + "_gt";
            } else {
                return this.param.ordering + "_lt";

            }
        } else {
            return "id_lt";
        }
    }


    loadFirst(id?:number): Observable<ResultObject[]> {
        if (this._url === undefined) {
            return of([]);
        }
        if (id) {
            //payload = payload.append(this.getGreaterThanParam(), id.toString());
            return this.http.get<ResultObject>(this._url + id.toString() + "/").pipe(
                map( result => {
                    //console.log("loadFirst", result);
                    if (this.param.ordering && this.param.ordering in result) {
                        this._ordering = (<any>result)[this.param.ordering];
                        return [result];
                    } else {
                        return [];
                    }
                }), catchError( this.handleError<ResultObject[]>("loadFirst failed", []) )
            );

        } else {
            let payload = this.getCommonPayload();
            payload = payload.append("ordering", <string>(this.param.ordering));
            //console.log("loadFirst", payload);
            return this.http.get<CustomHttpReply>(this._url, { params: payload}).pipe(
                map( result => {
                    //console.log("loadFirst", result);
                    let reply: CustomHttpReply = (<any>result) as CustomHttpReply;
                    this._urlNext = reply.next;
                    return reply.results;
                }), catchError( this.handleError<ResultObject[]>("loadFirst failed", []) )
            );
        }
    }

    loadNext(): Observable<ResultObject[]> {
        if (this._urlNext !== null) {
            return this.http.get<CustomHttpReply>(this._urlNext).pipe(
                map( result => {
                    let reply: CustomHttpReply = (<any>result) as CustomHttpReply;
                    this._urlNext = reply.next;
                    return reply.results;
                }), catchError( this.handleError<ResultObject[]>("loadMore failed", []) )
            );
        } else if (this._ordering !== undefined && !this._reachedNext) {
            if (this._url === undefined) {
                return of([]);
            }
            // We loaded only the first item and we need to initialise the next url
            let payload = this.getCommonPayload();
            payload = payload.append(this.getGreaterThanParam(), this._ordering.toString());
            payload = payload.append("ordering", <string>(this.param.ordering));

            return this.http.get<CustomHttpReply>(this._url, { params: payload} ).pipe(
                map( result => {
                    let reply: CustomHttpReply = (<any>result) as CustomHttpReply;
                    this._urlNext = reply.next;
                    if (this._urlNext === null) {
                        // We need to detect this usecase so we can then loop from the beginning
                        this._reachedNext = true;
                    }
                    return reply.results;
                }), catchError( this.handleError<ResultObject[]>("loadFirst failed", []) )
            );
        } else {
            if (this._url === undefined) {
                return of([]);
            }
            // We reached the end of normal load. We need to load the first pictures (without the <ordering>_gt set)
            let payload = this.getCommonPayload();
            payload = payload.append("ordering", <string>(this.param.ordering));

            return this.http.get<CustomHttpReply>(this._url, { params: payload} ).pipe(
                map( result => {
                    let reply: CustomHttpReply = (<any>result) as CustomHttpReply;
                    this._urlNext = reply.next;
                    return reply.results;
                }), catchError( this.handleError<ResultObject[]>("loadFirst failed", []) )
            );
        }
    }

    loadPrevious(): Observable<ResultObject[]> {
        if (this._urlPrevious !== null) {
            return this.http.get<CustomHttpReply>(this._urlPrevious).pipe(
                map( result => {
                    let reply: CustomHttpReply = (<any>result) as CustomHttpReply;
                    this._urlPrevious = reply.next;
                    return reply.results;
                }), catchError( this.handleError<ResultObject[]>("loadPrevious failed", []) )
            );
        } else if (this._ordering !== undefined && !this._reachedPrevious) {
            if (this._url === undefined) {
                return of([]);
            }
            // We need to load with <ordering>_lt set and reverse_ordering
            let payload = this.getCommonPayload();
            payload = payload.append(this.getLessThanParam(), this._ordering.toString());
            payload = payload.append("ordering", <string>(this.param.reverseOrdering));
            return this.http.get<CustomHttpReply>(this._url, { params: payload} ).pipe(
                map( result => {
                    let reply: CustomHttpReply = (<any>result) as CustomHttpReply;
                    this._urlPrevious = reply.next;
                    if (this._urlPrevious === null) {
                        // We need to detect this usecase so we can then loop from the end
                        this._reachedPrevious = true;
                    }
                    return reply.results.reverse();
                }), catchError( this.handleError<ResultObject[]>("loadPrevious failed", []) )
            );
        } else {
            if (this._url === undefined) {
                return of([]);
            }
            // We need to load with <ordering>_gt set but in reverse ordering
            let payload = this.getCommonPayload();
            if (this._ordering !== undefined) {
                payload = payload.append(this.getGreaterThanParam(), this._ordering.toString());
            }
            payload = payload.append("ordering", <string>(this.param.reverseOrdering));

            return this.http.get<CustomHttpReply>(this._url, { params: payload} ).pipe(
                map( result => {
                    let reply: CustomHttpReply = (<any>result) as CustomHttpReply;
                    this._urlPrevious = reply.previous;
                    return reply.results.reverse();
                }), catchError( this.handleError<ResultObject[]>("loadPrevious failed", []) )
            );
        }
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    private getCommonPayload(): HttpParams {
        let payload: HttpParams = new HttpParams();

        for (let arg in this.param.extra) {
            payload = payload.append(arg, this.param.extra[arg]);
        }

        return payload;
    }
}
