import { TestBed, inject } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { of }         from 'rxjs/observable/of';
import 'rxjs/add/operator/toPromise';

import { InfiniteService } from './infinite.service';
import { InfiniteLoaderService } from './infinite-loader.service';
import { ResultObject } from './result-object';
import { ParamObject } from './param-object';
import { WorkerService } from './worker.service';


class InfiniteLoaderServiceMock {
    size: number = 33;
    paginate: number = 10;
    next: number | null = null;
    previous: number | null = null;
    param: ParamObject;
    _id: number;

    init(param?: ParamObject): void {
        if (param) {
            this.param = param;
        } else {
            this.param = new ParamObject();
        }
        if (this.param.ordering === undefined) {
            this.param.ordering = "timestamp";
        }
        if (this.param.reverseOrdering === undefined) {
            this.param.reverseOrdering = this.param.ordering.substring(0,1) === "-"? this.param.ordering.substring(1) : "-" + this.param.ordering;
        }
    }

    loadFirst(id?: number): Observable<ResultObject[]> {
        if (id) {
            this._id = id;
        }
        //console.log("[mock]We init");
        if (this._id == undefined) {
            this.previous = null;
            this.next = 0;
        } else {
            this.previous = this._id;
            this.next = this._id;
        }
        //console.log("next=", this.next, "prev=", this.previous);
        return this.loadNext();
    }

    loadNext(): Observable<ResultObject[]> {
        if (this.next == null) {
            if (this._id) {
                // We loop from the beginning
                let results: ResultObject[] = [];
                for (var _i = 0; _i < Math.min(this.size, this.paginate); _i++) {
                    let obj: ResultObject = {
                        id: _i
                    }
                    results.push(obj);
                }
                if (this.paginate < this.size) {
                    this.next = this.paginate;
                } else {
                    this.next = null;
                }
                //console.log("next=", this.next, "prev=", this.previous);
                return of(results);
            } else {
                return of([]);
            }
        }else {
            let results: ResultObject[] = [];
            for (var _i = this.next; _i < Math.min(this.size, this.next+this.paginate); _i++) {
                let obj: ResultObject = {
                    id: _i
                }
                results.push(obj);

            }
            if (this.next+this.paginate < this.size) {
                this.next = this.next + this.paginate;
            } else {
                this.next = null;
            }
            //console.log("next=", this.next, "prev=", this.previous);
            return of(results);
        }

    }

    loadPrevious(): Observable<ResultObject[]> {
        //console.log("LoadPRevious : next=", this.next, "prev=", this.previous);
        if (this.previous == null) {
            if (this._id) {
                // We loop from the beginning
                let results: ResultObject[] = [];
                for (var _i = this._id; _i >= Math.max(0, this._id-this.paginate); _i--) {
                    let obj: ResultObject = {
                        id: _i
                    }
                    results.push(obj);

                }
                if (this._id-this.paginate >= 0) {
                    this.previous = this._id - this.paginate;
                } else {
                    this.previous = null;
                }
                return of(results);
            } else {
                // We loop from the end
                let results: ResultObject[] = [];
                for (var _i = this.size - 1; _i >= Math.max(0, this.size - 1 - this.paginate); _i--) {
                    let obj: ResultObject = {
                        id: _i
                    }
                    results.push(obj);
                }
                if (this.size - 1 - this.paginate >= 0) {
                    this.previous = this.size - 1 - this.paginate;
                } else {
                    this.previous = null;
                }
                return of(results);
            }
        }else {
            let results: ResultObject[] = [];
            for (var _i = this.previous; _i >= Math.max(0, this.previous-this.paginate); _i--) {
                let obj: ResultObject = {
                    id: _i
                }
                results.push(obj);

            }
            if (this.previous-this.paginate >= 0) {
                this.previous = this.previous - this.paginate;
            } else {
                this.previous = null;
            }
            return of(results);
        }

    }
}



describe('InfiniteLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [{
            provide: InfiniteLoaderService, useClass: InfiniteLoaderServiceMock
        }, WorkerService, InfiniteService]
    });
  });

  it('should load all items in proper order', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      loader.init();
      service.more();
      service.more();
      service.more();
      service.more();
      service.results.subscribe( results => {
          //console.log("subscribing to results", results);
          let i: number = 0;
          for (let item of results) {
              expect(item.id).toBe(i);
              i++;
          }
          expect(results.length).toBe(33);
      }
    );
  }));

  it('should load all items shifted by 3 in proper order', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      loader.init(param);
      service.getItem(3);
      service.more();
      service.more();
      service.more();
      service.results.subscribe( results => {
          //console.log("subscribing to results", results);
          let i: number = 3;
          for (let item of results) {
              expect(item.id).toBe(i % 33);
              i++;
          }
          expect(results.length).toBe(33);
      }
    );
  }));


  it('should get the item 3', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      loader.init(param);
      service.getItem(3).subscribe(item => {
          expect(item.id).toBe(3);
      });
  }));

  it('should get the item 3 already in memory', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      loader.init(param);
      service.more();
      service.results.subscribe( results => {
          service.getItem(3).subscribe(item => {
              expect(item.id).toBe(3);
          });
      });
  }));

  it('should get the item after 3 already in memory', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      loader.init(param);
      service.more();
      service.results.subscribe( results => {
          service.getNextItem(3).subscribe(item => {
              expect(item.id).toBe(4);
          });
      });
  }));

  it('should get the item after 9', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      loader.init(param);
      service.more();
      service.results.subscribe( results => {
          service.getNextItem(9).subscribe(item => {
              //console.log("next item = 10?", item);
              expect(item.id).toBe(10);
          });
      });
  }));


  it('should get the item after 33 and loop', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let fetchingNext = false;
      loader.init(param);
      service.more();
      service.more();
      service.more();
      service.more();
      service.results.subscribe( results => {
          if (!fetchingNext) {
              fetchingNext = true;
              //console.log("Subscribe now fetching more");
              service.getNextItem(32).subscribe(item => {
                  //console.log("next item =", item);
                  expect(item.id).toBe(0);
              });
          }
      });
  }));
  
  it('should get the item before 5', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      loader.init(param);
      service.more();
      service.results.subscribe( results => {
          service.getPreviousItem(5).subscribe(item => {
              //console.log("previous item = 4?", item);
              expect(item.id).toBe(4);
          });
      });
  }));

  it('should get the item before 0', inject([InfiniteService, InfiniteLoaderService], (service: InfiniteService, loader: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let looked = false;
      loader.init(param);
      service.more();
      service.results.subscribe( results => {
          //console.log(results);
          if (!looked) {
              looked = true;
              service.getPreviousItem(0).subscribe(item => {
                  //console.log("previous item = 32?", item);
                  expect(item.id).toBe(32);
              });
          }
      });
  }));

});
