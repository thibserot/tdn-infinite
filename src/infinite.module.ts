import { NgModule, ModuleWithProviders} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule }    from '@angular/common/http';

import { InfiniteLoaderService } from './infinite-loader.service';
import { InfiniteService } from './infinite.service';
import { WorkerService } from './worker.service';

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule
        
    ],
    declarations: [
    ],
    exports: [],
    providers: [InfiniteLoaderService, InfiniteService, WorkerService]
})

export class InfiniteModule {

}
