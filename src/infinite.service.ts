import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of }         from 'rxjs/observable/of';
import { defer }         from 'rxjs/observable/defer';
import { concat }         from 'rxjs/observable/concat';
import { catchError, map, tap, switchMap, distinctUntilChanged } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';

import { InfiniteLoaderService } from './infinite-loader.service';
import { WorkerService } from './worker.service';

import { ResultObject } from './result-object';
import { ParamObject } from './param-object';


@Injectable()
export class InfiniteService {
    private _results$:BehaviorSubject<ResultObject[]> = new BehaviorSubject<ResultObject[]>([]);

    private items:ResultObject[] = [];
    private itemsIdx: { [id: number]: number } = {};
    private overlap: boolean = false;
    private orderBy: string|undefined = undefined;
    private initDone: boolean | undefined;


    constructor(private loader: InfiniteLoaderService, private worker: WorkerService) { 
        this.worker.start();
    }

    get results(): Observable<ResultObject[]> {
        return this._results$.asObservable();
    }

    reset(): void {
        this.items.length = 0;
        this.itemsIdx = {};
        this.overlap = false;
        this.orderBy = undefined;
        this.initDone = undefined;
    }

    setOrderBy(orderBy:string) {
        this.orderBy = orderBy;
    }

    more(): Observable<any> {
        //console.log("InfiniteService - Next");
        if (!this.overlap) {
            let fetchedItem$:ReplaySubject<any> = new ReplaySubject<any>();
            if (this.initDone === undefined) {
                this.initDone = false;
                this.worker.addTask( { callback: this.fetchFirst.bind(this), obs$: fetchedItem$, idx: undefined });
            } else if (this.initDone){
                this.worker.addTask( { callback: this.fetchMore.bind(this), obs$: fetchedItem$, idx: undefined });
            }
            return fetchedItem$.asObservable();
        } else {
            return of();
        }

    }

    getItem(idx: number): Observable<ResultObject> {
        let fetchedItem$:ReplaySubject<ResultObject> = new ReplaySubject<ResultObject>();
        if (idx in this.itemsIdx) {
            fetchedItem$.next(this.items[this.itemsIdx[idx]]);
            fetchedItem$.complete();
        } else {
            this.initDone = false;
            this.worker.addTask( { callback: this.fetchItem.bind(this), obs$: fetchedItem$, idx: idx });
        }
        return fetchedItem$.asObservable();

    }

    getNextItem(idx: number): Observable<ResultObject> {
        let fetchedItem$:ReplaySubject<ResultObject> = new ReplaySubject<ResultObject>();
        if (idx in this.itemsIdx) {
            if (this.itemsIdx[idx] < this.items.length - 1) {
                //console.log("Next item: idx < length");
                fetchedItem$.next(this.items[this.itemsIdx[idx]+1]);
                fetchedItem$.complete();
            } else {
                // We need to check if we should loop or load more
                if (this.overlap) {
                    //console.log("Next item: Looping");
                    fetchedItem$.next(this.items[0]);
                    fetchedItem$.complete();
                } else {
                    //console.log("Next item: fetching more");
                    this.worker.addTask( { callback: this.fetchMore.bind(this), obs$: fetchedItem$, idx: idx });
                }

            }
        } else {
            //console.log("Next item: Error");
            //fetchedItem$.error("Invalid idx");
            // We can't throw error because the reference is lost and this need to be investigated deeper
            fetchedItem$.next(null);
            fetchedItem$.complete();
        }
        return fetchedItem$.asObservable();

    }

    getPreviousItem(idx: number): Observable<ResultObject> {
        let fetchedItem$:ReplaySubject<ResultObject> = new ReplaySubject<ResultObject>();
        if (idx in this.itemsIdx) {
            if (this.itemsIdx[idx] > 0) {
                //console.log("Previous item: idx > 0", idx, this.itemsIdx[idx],this.itemsIdx[idx]-1,this.items[this.itemsIdx[idx]-1]);
                //console.log(this.items);
                //console.log(this.itemsIdx);
                fetchedItem$.next(this.items[this.itemsIdx[idx]-1]);
                fetchedItem$.complete();
            } else {
                // We need to check if we should loop or load more
                if (this.overlap) {
                    //console.log("Previous item: Looping");
                    fetchedItem$.next(this.items[this.items.length - 1]);
                    fetchedItem$.complete();
                } else {
                    //console.log("Previous item: fetching more");
                    this.worker.addTask( { callback: this.fetchPreviousMore.bind(this), obs$: fetchedItem$, idx: idx });
                }
            }
        } else {
            //console.log("Previous item: Error");
            //fetchedItem$.error("Invalid idx");
            // We can't throw error because the reference is lost and this need to be investigated deeper
            fetchedItem$.next(null);
            fetchedItem$.complete();
        }
        return fetchedItem$.asObservable();

    }



    private fetchFirst(fetchedItem$:ReplaySubject<ResultObject>, idx:number|undefined): void {
        this.loader.loadFirst().pipe(catchError(this.handleError('fetchFirst', [])))
        .subscribe( items => {
            //console.log("fetchFirst subscribe", items);
            let overlap: boolean = this.append(items as ResultObject[]);

            if (overlap) {
                this.overlap = true;
            }
            this.initDone = true;
            this.pushItems();
            fetchedItem$.complete();

        });
    }
    private fetchMore(fetchedItem$:ReplaySubject<ResultObject>, idx:number|undefined ): void {

        if (idx !== undefined && idx in this.itemsIdx) {
            if (this.itemsIdx[idx] < this.items.length - 1) {
                fetchedItem$.next(this.items[this.itemsIdx[idx]+1]);
                fetchedItem$.complete();
                return;
            } else {
                // We need to check if we should loop or load more
                if (this.overlap) {
                    //console.log("Next item: Looping");
                    fetchedItem$.next(this.items[0]);
                    fetchedItem$.complete();
                    return;
                }
            }
        }

        this.loader.loadNext().pipe(catchError(this.handleError('fetchMore', [])))
        .subscribe( items => {
            //console.log("fetchMore subscribe", items);
            let overlap: boolean = this.append(items as ResultObject[])
            if (overlap) {
                this.overlap = true;
            }

            if (fetchedItem$) {
                if (idx) {
                    //console.log("We notify the next fetchedItem");
                    if (this.itemsIdx[idx] < this.items.length - 1) {
                        fetchedItem$.next(this.items[this.itemsIdx[idx]+1]);
                    } else {
                        fetchedItem$.next(this.items[0]);
                    }
                }
                fetchedItem$.complete();

            }
            this.pushItems();
        });
    }

    private fetchPreviousMore(fetchedItem$:ReplaySubject<ResultObject>, idx:number|undefined ): void {


        if (idx !== undefined && idx in this.itemsIdx) {
            if (this.itemsIdx[idx] > 0) {
                //console.log("Previous item: idx > 0");
                fetchedItem$.next(this.items[this.itemsIdx[idx]-1]);
                fetchedItem$.complete();
                return;
            } else {
                // We need to check if we should loop or load more
                if (this.overlap) {
                    //console.log("Previous item: Looping");
                    fetchedItem$.next(this.items[this.items.length - 1]);
                    fetchedItem$.complete();
                    return;
                }
            }
        }
           
        this.loader.loadPrevious().pipe(catchError(this.handleError('fetchPreviousMore', [])))
        .subscribe( items => {
            //console.log("fetchPreviousMore subscribe", items);
            // TODO: Determine if when items is empty whether we should consider that we have overlap or not...
            let overlap: boolean = this.prepend(items as ResultObject[])
            if (overlap) {
                this.overlap = true;
            }

            if (idx && fetchedItem$) {
                if (this.itemsIdx[idx] > 0) {
                    fetchedItem$.next(this.items[this.itemsIdx[idx]-1]);
                } else {
                    fetchedItem$.next(this.items[this.items.length - 1]);
                }
                fetchedItem$.complete();
            }
            this.pushItems();
        });
    }

    private fetchItem(fetchedItem$:ReplaySubject<ResultObject>,idx: number): void {
        if (idx in this.itemsIdx) {
            fetchedItem$.next(this.items[this.itemsIdx[idx]]);
            fetchedItem$.complete();
        } else {
            this.loader.loadFirst(idx).pipe(catchError(this.handleError('fetchItem', [])))
            .subscribe( items => {
                //console.log("fetchItem subscribe", items);
                let overlap: boolean = this.append(items as ResultObject[]);

                if (overlap) {
                    this.overlap = true;
                }


                if (idx in this.itemsIdx) {
                    //console.log("We found the item");
                    fetchedItem$.next(this.items[this.itemsIdx[idx]]);
                } else {
                    //console.log("We didn't found the item");
                    //fetchedItem$.error("Item doesn't exists");
                    // We can't throw error because the reference is lost and this need to be investigated deeper
                    fetchedItem$.next(null);
                    // Image doesn't exist, we throw an error
                }
                fetchedItem$.complete();

                this.initDone = true;

                this.pushItems();

            });
        }
    }

    private pushItems(): void {
        if (this.orderBy !== undefined) {
            this._results$.next(this.items.slice(0).sort((tt1, tt2) => {
                let orderBy = this.orderBy as string;
                let t1: any = tt1;
                let t2: any = tt2;
                if (t1[orderBy] > t2[orderBy]) {
                    return 1;
                }
                if (t1[orderBy] < t2[orderBy]) {
                    return -1;
                }
                return 0;
            }));
        } else {
            this._results$.next(this.items);
        }
    }

    private append(items: ResultObject[]): boolean {
        let index:number = this.items.length;
        let data: ResultObject[] = items;
        let overlap: boolean = false;

        for (let item of data) {
            if (!(item.id in this.itemsIdx)) {
                index++;
                this.itemsIdx[item.id] = index;
                this.items.splice(index, 0, item);
            } else {
                overlap = true;
                break;
            }
        }
        this.rebuildIndex();
        return overlap;

    }

    private rebuildIndex() {
        let idx: number = 0;
        for (let item of this.items) {
            this.itemsIdx[item.id] = idx;
            idx++;
        }
    }

    private prepend(items: ResultObject[]): boolean {
        let index:number = 0;
        let data: ResultObject[] = items;
        let overlap: boolean = false;

        for (let item of data) {
            if (!(item.id in this.itemsIdx)) {
                this.itemsIdx[item.id] = index;
                this.items.splice(index, 0, item);
            } else {
                overlap = true;
                break;
            }
        }

        this.rebuildIndex();

        return overlap;
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("We got an error", result);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
