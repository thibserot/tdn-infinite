import { Injectable, Inject } from '@angular/core';
import { of }         from 'rxjs/observable/of';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { exhaustMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';

export interface Item {
    callback: Function;
    obs$: ReplaySubject<any>;
    idx: number|undefined;
}


@Injectable()
export class WorkerService {
    started: boolean = false;
    busy: boolean = true;
    queue: Item[] = [];

    constructor() { }

    start(): void {
        this.started = true;
        this.busy = false;
        this.process();
    }

    reset(): void {
        this.started = false;
        this.busy = true;
        this.queue = [];
    }

    addTask(item:Item): void {
        this.queue.push(item);
        this.process();
    }

    process(): void {
        //console.log(this.started, this.busy, this.queue);
        if (!this.started || this.busy || this.queue.length === 0) {
            return;
        }
        this.busy= true;
        this.processItem(<Item>(this.queue.pop()));
    }

    processItem(item: Item): void {
        //console.log("Processing", item);
        let obs$: Observable<any> = item.obs$.asObservable();
        obs$.finally(() => {
            this.busy = false;
            this.process();
        }).subscribe( () => {});
        item.callback(item.obs$, item.idx);
    }

    
    isBusy(): boolean {
        return this.busy;
    }
}
