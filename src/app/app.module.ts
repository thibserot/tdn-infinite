import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 

import { AppComponent } from './app.component';
import { InfiniteModule } from '../infinite.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    InfiniteModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
