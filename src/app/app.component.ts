import { Component, Injectable, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { InfiniteLoaderService } from '../infinite-loader.service';
import { InfiniteService } from '../infinite.service';
import { ResultObject } from '../result-object';
import { ParamObject } from '../param-object';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
      InfiniteLoaderService,
      InfiniteService
  ]
})
export class AppComponent implements OnInit {
    title = 'app';
    @Input() id: number = 2440;
    results$: Observable<ResultObject[]>;
    picture$: Observable<ResultObject>;
    picture: ResultObject;

    constructor(private loader: InfiniteLoaderService, private infiniteService: InfiniteService) {
    }
    
    ngOnInit() {
        // Should probably be moved to ngOnInit
        let param: ParamObject = new ParamObject();
        param.ordering = "added";
        this.loader.init(param);
        this.infiniteService.reset();
        this.results$ = this.infiniteService.results;
        this.infiniteService.setOrderBy("id");
        this.loader.setUrl("http://localhost:8000/api/pictures/");
    }


    next() {
        this.infiniteService.more();
    }

    getPicture() {
        this.picture$ = this.infiniteService.getItem(this.id);
        this.updateId();
    }

    nextPicture() {
        this.picture$ = this.infiniteService.getNextItem(this.id);
        this.updateId();

    }

    previousPicture() {
        this.picture$ = this.infiniteService.getPreviousItem(this.id);
        this.updateId();

    }
    updateId() {
        this.picture$.subscribe(item => {
            if (item) {
                this.id = item.id;
                this.picture = item;
            }
        })
    }
    updatePicture() {
        this.loader.put(this.picture).then(rsp => {
            console.log("Response =", rsp);
        }).catch(error => {
            console.error("Error = ", error);
        });
    }

    deletePicture() {
        this.loader.delete(this.picture).then(rsp => {
            console.log("Response =", rsp);
        }).catch(error => {
            console.error("Error = ", error);
        });
    }


}
