import { TestBed, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { InfiniteLoaderService, CustomHttpReply } from './infinite-loader.service';
import { ParamObject } from './param-object';
import { ResultObject } from './result-object';

describe('InfiniteLoaderService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [InfiniteLoaderService]
        });
    });

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

  it('should be created', inject([HttpClient, HttpTestingController, InfiniteLoaderService], (http: HttpClient, httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      expect(service).toBeTruthy();
  }));

  it('should submit a request from the start', inject([HttpTestingController, InfiniteLoaderService], (httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let response: ResultObject[] = [];
      service.init(param);
      service.setUrl("/url");
      service.loadFirst().subscribe( results => {
          response = results;
      });

      let reply: CustomHttpReply = {
          next: "/next",
          previous: "/previous",
          count: 5,
          results: [ 
              {id: 0},
              {id: 1},
              {id: 2},
              {id: 3},
              {id: 4}
          ]
      };
      httpMock.expectOne("/url?ordering=id").flush(reply);
      expect(response).toEqual(reply.results);
  }));

  it('should submit a request from the id 3', inject([HttpTestingController, InfiniteLoaderService], (httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let response: ResultObject[] = [];
      service.init(param);
      service.setUrl("/url");
      service.loadFirst(3).subscribe( results => {
          response = results;
      });

      let reply= {
            id: 3
      };

      httpMock.expectOne("/url3/").flush(reply);
      expect(response).toEqual([reply]);
  }));


  it('should load the next batch of item', inject([HttpTestingController, InfiniteLoaderService], (httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let response: ResultObject[] = [];
      service.init(param);
      service.setUrl("/url");
      service.loadFirst().subscribe( _ => {
          service.loadNext().subscribe( results => {
              response = results;
          });
      });

      let reply: CustomHttpReply = {
          next: "/next",
          previous: "/previous",
          count: 5,
          results: [ 
          ]
      };

      let replyNext: CustomHttpReply = {
          next: "/next",
          previous: "/previous",
          count: 5,
          results: [ 
              {id: 0},
              {id: 1},
              {id: 2},
              {id: 3},
              {id: 4}
          ]
      };
      httpMock.expectOne("/url?ordering=id").flush(reply);
      httpMock.expectOne("/next").flush(replyNext);
      expect(response).toEqual(replyNext.results);
  }));

  it('should load the next batch of item from beginning', inject([HttpTestingController, InfiniteLoaderService], (httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let response: ResultObject[] = [];
      service.init(param);
      service.setUrl("/url");
      service.loadFirst(4).subscribe( _ => {
          service.loadNext().subscribe( results => {
              response = results;
          });
      });

      let reply = {
          id: 4
      };

      let replyNext: CustomHttpReply = {
          next: "/next",
          previous: "/previous",
          count: 5,
          results: [ 
              {id: 0},
              {id: 1},
              {id: 2},
              {id: 3},
              {id: 4}
          ]
      };
      httpMock.expectOne("/url4/").flush(reply);
      httpMock.expectOne("/url?id_gt=4&ordering=id").flush(replyNext);
      expect(response).toEqual(replyNext.results);
  }));

  it('should load the previous batch of item from beginning', inject([HttpTestingController, InfiniteLoaderService], (httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let response: ResultObject[] = [];
      service.init(param);
      service.setUrl("/url");
      service.loadFirst().subscribe( _ => {
          service.loadPrevious().subscribe( results => {
              response = results;
          });
      });

      let reply: CustomHttpReply = {
          next: "/next",
          previous: null,
          count: 5,
          results: [ 
          ]
      };

      let replyPrevious: CustomHttpReply = {
          next: "/next",
          previous: "/previous",
          count: 5,
          results: [ 
              {id: 3},
              {id: 2},
              {id: 1},
              {id: 0}
          ]
      };
      let expectedReply: ResultObject[] = [
              {id: 0},
              {id: 1},
              {id: 2},
              {id: 3}
      ];
      httpMock.expectOne("/url?ordering=id").flush(reply);
      httpMock.expectOne("/url?ordering=-id").flush(replyPrevious);
      expect(response).toEqual(expectedReply);
  }));

  it('should load the previous batch of item before id=3', inject([HttpTestingController, InfiniteLoaderService], (httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let response: ResultObject[] = [];
      service.init(param);
      service.setUrl("/url");
      service.loadFirst(3).subscribe( _ => {
          service.loadPrevious().subscribe( results => {
              response = results;
          });
      });

      let reply = {
          id: 3
      };

      let replyPrevious: CustomHttpReply = {
          next: "/next",
          previous: "/previous",
          count: 5,
          results: [ 
              {id: 2},
              {id: 1},
              {id: 0}
          ]
      };
      let expectedReply: ResultObject[] = [
              {id: 0},
              {id: 1},
              {id: 2}
      ];
      httpMock.expectOne("/url3/").flush(reply);
      //httpMock.expectOne(req => { console.log(req); return true;}).flush(reply);
      httpMock.expectOne("/url?id_lt=3&ordering=-id").flush(replyPrevious);
      expect(response).toEqual(expectedReply);
  }));

  it('should load the previous batch of item after id=3 and loop to the end"', inject([HttpTestingController, InfiniteLoaderService], (httpMock:HttpTestingController, service: InfiniteLoaderService) => {
      let param: ParamObject = new ParamObject();
      let response: ResultObject[] = [];
      service.init(param);
      service.setUrl("/url");
      service.loadFirst(3).subscribe( _ => {
          service.loadPrevious().subscribe( _ => {
              service.loadPrevious().subscribe( results => {
                  response = results;
              });
          });
      });

      let reply = {
          id: 3
      };

      let replyPrevious: CustomHttpReply = {
          next: null,
          previous: null,
          count: 5,
          results: [ 
              {id: 2},
              {id: 1},
              {id: 0}
          ]
      };

      let replyPreviousEnd: CustomHttpReply = {
          next: "/next",
          previous: "/previous",
          count: 5,
          results: [ 
              {id: 7},
              {id: 6},
              {id: 5}
          ]
      };

      let expectedReply: ResultObject[] = [
              {id: 5},
              {id: 6},
              {id: 7}
      ];
      httpMock.expectOne("/url3/").flush(reply);
      httpMock.expectOne("/url?id_lt=3&ordering=-id").flush(replyPrevious);
      httpMock.expectOne("/url?id_gt=3&ordering=-id").flush(replyPreviousEnd);
      //httpMock.expectOne(req => {
      //    console.log(req);
      //    return true;
      //}).flush(replyPrevious);
      expect(response).toEqual(expectedReply);
  }));

});
