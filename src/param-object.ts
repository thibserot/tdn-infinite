export class ParamObject {
    ordering: string|undefined;
    reverseOrdering: string|undefined;
    extra: any = {};
}
