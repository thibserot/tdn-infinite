export default {
	input: 'dist/index.js',
	sourceMap: false,
    output: {
        name: 'ng.tdn-infinite',
        file: 'dist/bundles/tdn-infinite.umd.js',
        format: 'umd'
    },
	globals: {
		'@angular/core': 'ng.core',
		'@angular/common': 'ng.common',
		'@angular/common/http': 'ng.common.http',
		'rxjs/Observable': 'Rx',
		'rxjs/ReplaySubject': 'Rx',
		'rxjs/BehaviorSubject': 'Rx',
		'rxjs/operators': 'Rx',
		'rxjs/observable/of': 'Rx',
		'rxjs/add/operator/map': 'Rx.Observable.prototype',
		'rxjs/add/operator/finally': 'Rx.Observable.prototype',
		'rxjs/add/operator/catch': 'Rx.Observable.prototype',
		'rxjs/add/operator/toPromise': 'Rx.Observable.prototype'
	},
    external: [
		'@angular/core',
		'@angular/common',
		'@angular/common/http',
		'rxjs/Observable',
		'rxjs/ReplaySubject',
		'rxjs/BehaviorSubject',
		'rxjs/operators',
		'rxjs/add/operator/map',
		'rxjs/observable/of',
		'rxjs/add/operator/finally',
		'rxjs/add/operator/catch',
		'rxjs/add/operator/toPromise'
    ]
}
