# TDN Infinite

This project provides an interface with [Django-rest-framework](http://www.django-rest-framework.org/). It allows you to retrieve the content of a list from anywhere in the list and to infinitely loop over it.

Example of use case where it can be useful: Loading a galery of pictures

## Installation

```
npm install tdn-infinite
```

## How to use it?


## How to create a new delivery?

Using this excellent [tutorial](https://github.com/davguij/angular-npm-module-seed), you simply need to run:

```
npm install
npm run build
```

If you want to test it locally, go in the dist folder and run `npm link`. Then go to your test app root dir and do `npm link tdn-infinite`
Re-start your server with following command: `ng serve --preserve-symlinks`

## Publish to NPM

Go to the dist folder and run:
```
npm login
npm publish
```
